"~/.vim/plugin/soa.vim
"--- .../plugins/named.vim ----
"from https://blog.rabin.io/linux/auto-increment-serial-number-zone-file-edit-with-vim
"Author: Jakobus Schürz
"Date: 27. August 2021

function! UPDSERIAL(num)
return a:num+1
endfunction

command Soa :%s/\(\s*\)\([0-9]*\)\(\s*;\s*serial\)/\=submatch(1) . UPDSERIAL(submatch(2)) . submatch(3)/gc
"---- eof ---------------------

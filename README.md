# Autoincrement Serial-Number in bind zone-files

die Datei ~/.vimrc ist um folgende Zeile zu ergänzen

```
autocmd BufWritePre /etc/bind/vpn/db.* Soa
```

damit wird die Seriennummer eines Zonefiles für das vpn immer um 1 erhöht.

die Zeile mit der Seriennummer muss folgendem Muster entsprechen
```
         12345 ; serial
```
wobei anfangs beliebig viele Leerzeichen und zwischen der Seriennummer, dem Strichpunkt und "serial" sein dürfen. "serial" muss klein geschrieben sein. Die Seriennummer wird immer um 1 erhöht. 
